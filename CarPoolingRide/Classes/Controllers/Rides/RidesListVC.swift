//
//  HomeVC.swift
//  CarPoolingRide
//
//  Created by user on 29/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit
//import GoogleMaps


class RidesListVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var viewList: UIView!
    var arrList = ["Search Ride","Offer Ride","Current Ride","Ride History"]
//    override func loadView() {
//      // Create a GMSCameraPosition that tells the map to display the
//      // coordinate -33.86,151.20 at zoom level 6.
//      let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
//      let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//      view = mapView
//
//      // Creates a marker in the center of the map.
//      let marker = GMSMarker()
//      marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//      marker.title = "Sydney"
//      marker.snippet = "Australia"
//      marker.map = mapView
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewList.layer.cornerRadius = 32.0
        // Do any additional setup after loading the view.
    }
    
    @objc func buttonRequestClicked(_ sender : UIButton){
        let vc = self.storyboard?.instantiateViewController(identifier: "RequestedRideVC") as! RequestedRideVC
       self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrList.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let separatorLineView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 25))
        /// change size as you need.
        separatorLineView.backgroundColor = UIColor.clear
        // you can also put image here
        return separatorLineView

    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // create a new cell if needed or reuse an old one
//        let cell:HomeCell = (tableView.dequeueReusableCell(withIdentifier: "HomeCell") as HomeCell?)!?

        let cell:RideCell = tableView.dequeueReusableCell(withIdentifier: "RideCell") as! RideCell
        cell.contentView.layer.cornerRadius = 18
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor.copy(alpha: 1)
        cell.contentView.layer.borderWidth = 0.5
 
        cell.selectionStyle = .none
        
//        cell.labelTitle.text = arrList[indexPath.section]
//        let strImageName = String(format: "ride_icon%d", indexPath.section)
//        cell.imageViewRideSelect.image = UIImage(named: strImageName)
        cell.imageViewProfile.layer.cornerRadius = cell.imageViewProfile.frame.size.width/2
        cell.buttonRequest.layer.cornerRadius = 6.0
//        cell.buttonRequest.addTarget(self, action: #selector(buttonRequestClicked), for: .touchUpInside)
        cell.buttonRequest.addTarget(self, action: #selector(buttonRequestClicked(_:)), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }


}



extension UIView {

 
}
