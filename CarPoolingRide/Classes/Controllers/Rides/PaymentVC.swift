//
//  PaymentVC.swift
//  CarPoolingRide
//
//  Created by Abhishek Patel on 30/01/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit

class PaymentVC: BaseViewController {

    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelRate: UILabel!
    @IBOutlet weak var viewRate: UIView!
    @IBOutlet weak var viewList: UIView!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var textViewComment: UITextView!
    @IBOutlet weak var viewSelect: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonSubmit.layer.cornerRadius = CGFloat(Float(Constants.kButtonRadius))
        self.textViewComment.layer.borderColor = UIColor.lightGray.cgColor
        self.viewSelect.layer.borderColor = UIColor.lightGray.cgColor
        self.textViewComment.layer.borderWidth = 1.0
        self.viewSelect.layer.borderWidth = 1.0
        self.viewSelect.layer.cornerRadius = CGFloat(Float(6))
        self.textViewComment.layer.cornerRadius = CGFloat(Float(6))

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
