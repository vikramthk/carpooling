//
//  HomeVC.swift
//  CarPoolingRide
//
//  Created by user on 29/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit
//import GoogleMaps


class BookRideVC: BaseViewController{

    //@IBOutlet weak var buttonReq: UIButton!

   // @IBOutlet weak var viewList: UIView!
  //  @IBOutlet weak var viewPrice: UIView!
   // @IBOutlet weak var labelFinalPrice: UILabel!
    var arrList = ["Search Ride","Offer Ride","Current Ride","Ride History"]
    
    @IBOutlet weak var buttonToday: UIButton!
    @IBOutlet weak var buttonTomorrow: UIButton!
    @IBOutlet weak var viewH: UIView!
    @IBOutlet weak var viewM: UIView!
    @IBOutlet weak var viewAMPM: UIView!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    @IBOutlet weak var buttonSelect: UIButton!
    //    override func loadView() {
//      // Create a GMSCameraPosition that tells the map to display the
//      // coordinate -33.86,151.20 at zoom level 6.
//      let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
//      let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//      view = mapView
//
//      // Creates a marker in the center of the map.
//      let marker = GMSMarker()
//      marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//      marker.title = "Sydney"
//      marker.snippet = "Australia"
//      marker.map = mapView
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.viewList.layer.cornerRadius = 32.0
        // Do any additional setup after loading the view.
      //  viewPrice.layer.cornerRadius = 12.0
       buttonSubmit.layer.cornerRadius = 10.0
        self.setCustomValues()
    }
    
    func setCustomValues() {
        buttonToday.layer.cornerRadius = 4.0
        buttonTomorrow.layer.cornerRadius = 4.0
        buttonSelect.layer.cornerRadius = 4.0
        buttonSelect.layer.borderWidth = 0.5
        buttonSelect.layer.borderColor = UIColor.lightGray.cgColor
        viewH.layer.cornerRadius = 4.0
        viewM.layer.cornerRadius = 4.0
        viewAMPM.layer.cornerRadius = 4.0
    }
    
    @IBAction func rr(_ sender: Any) {
    }
    @IBAction func t(_ sender: Any) {
    }
    @IBAction func buttonSearch(_ sender: Any) {
        let alertVC = self.storyboard?.instantiateViewController(identifier: "AlertVC") as! AlertVC
        //self.present(alertVC, animated: true, completion: nil)
        self.navigationController?.pushViewController(alertVC, animated: false)
    }
    
    @IBAction func buttonSearchClicked(_ sender: Any) {
//        let alertVC = self.storyboard?.instantiateViewController(identifier: "AlertVC") as! AlertVC
//        self.present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func buttonSelectClicked(_ sender: Any) {
        
    }
    

}



extension UIView {

//   func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
//       layer.masksToBounds = false
//       layer.shadowColor = color.cgColor
//       layer.shadowOpacity = opacity
//       layer.shadowOffset = offSet
//       layer.shadowRadius = radius
//
//       layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//       layer.shouldRasterize = true
//       layer.rasterizationScale = scale ? UIScreen.main.scale : 1
//     }
}
