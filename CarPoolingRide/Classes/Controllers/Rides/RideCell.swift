//
//  RideCell.swift
//  CarPoolingRide
//
//  Created by user on 15/12/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

class RideCell: UITableViewCell {
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelRate: UILabel!
    
    @IBOutlet weak var labelSeats: UILabel!
    @IBOutlet weak var labelStartLoc: UILabel!
    @IBOutlet weak var labelEndLoc: UILabel!
    @IBOutlet weak var textFieldStart: UITextField!
    @IBOutlet weak var textFieldEnd: UITextField!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var buttonRequest: UIButton!
    @IBOutlet weak var buttonFlip: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
