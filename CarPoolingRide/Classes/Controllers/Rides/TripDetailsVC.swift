//
//  HomeVC.swift
//  CarPoolingRide
//
//  Created by user on 29/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit
//import GoogleMaps


class TripDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var buttonStart: UIButton!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewPopupUI: UIView!

//    @IBOutlet weak var viewList: UIView!
    var arrList : NSMutableArray = NSMutableArray()
//    override func loadView() {
//      // Create a GMSCameraPosition that tells the map to display the
//      // coordinate -33.86,151.20 at zoom level 6.
//      let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
//      let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//      view = mapView
//
//      // Creates a marker in the center of the map.
//      let marker = GMSMarker()
//      marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//      marker.title = "Sydney"
//      marker.snippet = "Australia"
//      marker.map = mapView
    
    
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//      self.viewList.layer.cornerRadius = 32.0
        let dict1 = ["name":"Mark","date": "4th Jan 2020" ,"isExpanded":"1"]
        let dict2 = ["name":"John","date":  "5th Feb 2020" ,"isExpanded":"0"]
        let dict3  = ["name":"James", "date": "6th Mar 2020","isExpanded":"0"]
        arrList.add(dict1)
        //arrList.add(dict2)
        //arrList.add(dict3)
         buttonStart.layer.cornerRadius = 8.0
         buttonCancel.layer.cornerRadius = 8.0
         //self.setBorderForTextField(buttonCancel)
    }

    @IBAction func buttonCancelClicked(_ sender: Any) {
        //CancelRideVC
        let popupVC = storyboard?.instantiateViewController(withIdentifier: "CancelRideVC") as! CancelRideVC
              view.addSubview(popupVC.view)
               addChild(popupVC)

    }
    
   
    @IBAction func buttonStartClicked(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func buttonExpandClicked(_ sender : Any){
        
        let button = sender as! UIButton
        let tag = button.tag
        var dictSelected : [String: String] = self.arrList[tag] as! [String : String]
        //var new = dict.updateValue("1", forKey: "isExpanded")
//        dictSelected["isExpanded"] = "1"
            //  self.arrList.replaceObject(at: tag, with: dictSelected)
         
        
        for (index, dict) in self.arrList.enumerated()
        {
            var tempDict = dict as! [String : String]
            if tempDict == dictSelected {
                
                if dictSelected["isExpanded"] == "1"{
                    dictSelected["isExpanded"] = "0"
                }else{
                    dictSelected["isExpanded"] = "1"
                }
                self.arrList.replaceObject(at: tag, with: dictSelected)
            }else{
                tempDict["isExpanded"] = "0"
                self.arrList.replaceObject(at: index, with: tempDict)
            }
        }
         
        tableViewList.reloadData()

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dict = self.arrList[indexPath.row]  as! [String : String]
        let strISExpanded = dict["isExpanded"]
        
        if strISExpanded == "1" {
            return 455
        }else{
            return 200
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let separatorLineView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 25))
        /// change size as you need.
        separatorLineView.backgroundColor = UIColor.clear
        // you can also put image here
        return separatorLineView

    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // create a new cell if needed or reuse an old one
//        let cell:HomeCell = (tableView.dequeueReusableCell(withIdentifier: "HomeCell") as HomeCell?)!?

        let cell:RequestedRideCell = tableView.dequeueReusableCell(withIdentifier: "RequestedRideCell") as! RequestedRideCell
        cell.contentView.layer.cornerRadius = 18
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor.copy(alpha: 1)
        cell.contentView.layer.borderWidth = 0.5
 
        cell.selectionStyle = .none
        //cell.buttonExpand.tag = indexPath.section
       // cell.buttonExpand.addTarget(self, action: #selector(buttonExpandClicked), for: .touchUpInside)
       // cell.buttonCancel.tag = indexPath.section
         //cell.buttonCancel.addTarget(self, action: #selector(buttonCancelClicked), for: .touchUpInside)

        let dict = self.arrList[indexPath.row] as! [String : String]
        let strISExpanded = dict["isExpanded"]
        
        if strISExpanded == "1" {
            cell.layoutConstraintLineHeight.constant = 75
            cell.viewMulti.isHidden = false
        }else{
            cell.layoutConstraintLineHeight.constant = 0
            cell.viewMulti.isHidden = true
        }
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.borderWidth = 0.0;

//        cell.labelTitle.text = arrList[indexPath.section]
//        let strImageName = String(format: "ride_icon%d", indexPath.section)
//        cell.imageViewRideSelect.image = UIImage(named: strImageName)
        cell.imageViewProfile.layer.cornerRadius = cell.imageViewProfile.frame.size.width/2
        //cell.buttonRequest.layer.cornerRadius = 6.0
        return cell
    }
    



}

 
