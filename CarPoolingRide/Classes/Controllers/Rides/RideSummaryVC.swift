//
//  HomeVC.swift
//  CarPoolingRide
//
//  Created by user on 29/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit
//import GoogleMaps


class RideSummaryVC: UIViewController{

    @IBOutlet weak var buttonReq: UIButton!

    @IBOutlet weak var viewList: UIView!
    @IBOutlet weak var viewPrice: UIView!
    @IBOutlet weak var labelFinalPrice: UILabel!
    var arrList = ["Search Ride","Offer Ride","Current Ride","Ride History"]
    
    @IBOutlet weak var buttonBack: UIButton!
    
//    override func loadView() {
//      // Create a GMSCameraPosition that tells the map to display the
//      // coordinate -33.86,151.20 at zoom level 6.
//      let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
//      let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//      view = mapView
//
//      // Creates a marker in the center of the map.
//      let marker = GMSMarker()
//      marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//      marker.title = "Sydney"
//      marker.snippet = "Australia"
//      marker.map = mapView
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewList.layer.cornerRadius = 32.0
        // Do any additional setup after loading the view.
        viewPrice.layer.cornerRadius = 12.0
        buttonReq.layer.cornerRadius = 10.0
        buttonBack.layer.cornerRadius = 10.0
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonRequestClicked(_ sender: Any) {
            let vc = self.storyboard?.instantiateViewController(identifier: "RidesListVC") as! RidesListVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    


}



extension UIView {

//   func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
//       layer.masksToBounds = false
//       layer.shadowColor = color.cgColor
//       layer.shadowOpacity = opacity
//       layer.shadowOffset = offSet
//       layer.shadowRadius = radius
//
//       layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//       layer.shouldRasterize = true
//       layer.rasterizationScale = scale ? UIScreen.main.scale : 1
//     }
}
