//
//  RequestedRideCell.swift
//  CarPoolingRide
//
//  Created by user on 05/01/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit

class RequestedRideCell: UITableViewCell {

    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!

    @IBOutlet weak var buttonExpand: UIButton!
    @IBOutlet weak var labelFrom: UILabel!
    @IBOutlet weak var labelTo: UILabel!
    @IBOutlet weak var labelFromMulti: UILabel!
    @IBOutlet weak var labelToMulti: UILabel!

    @IBOutlet weak var labelSeats: UILabel!
    @IBOutlet weak var labelDollar: UILabel!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var imageViewStatus: UIImageView!
    
    @IBOutlet weak var imageViewFromIcon: UIImageView!
    @IBOutlet weak var imageViewToIcon: UIImageView!

    @IBOutlet weak var viewMulti: UIView!
    @IBOutlet weak var layoutConstraintLineHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
     }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
