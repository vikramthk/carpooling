//
//  HomeVC.swift
//  CarPoolingRide
//
//  Created by user on 29/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit
//import GoogleMaps


class HomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var buttonHome: UIButton!
    @IBOutlet weak var buttonEarnings: UIButton!
    @IBOutlet weak var buttonNotification: UIButton!
    @IBOutlet weak var buttonProfile: UIButton!
    @IBOutlet weak var buttonSettings: UIButton!
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var viewList: UIView!
    var arrList = ["Search Ride","Offer Ride","Current Ride","Ride History"]
//    override func loadView() {
//      // Create a GMSCameraPosition that tells the map to display the
//      // coordinate -33.86,151.20 at zoom level 6.
//      let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
//      let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//      view = mapView
//
//      // Creates a marker in the center of the map.
//      let marker = GMSMarker()
//      marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//      marker.title = "Sydney"
//      marker.snippet = "Australia"
//      marker.map = mapView
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewList.layer.cornerRadius = 32.0
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonHomeTab1Clicked(_ sender: Any) {
        var image = UIImage(named: "home_active_icon")
        buttonHome.setImage(image, for: .normal)
        
        image = UIImage(named: "earnings_icon")
        buttonEarnings.setImage(image, for: .normal)

        image = UIImage(named: "notifications_icon")
        buttonNotification.setImage(image, for: .normal)

        image = UIImage(named: "settings_icon")
        buttonSettings.setImage(image, for: .normal)

    }
    
    @IBAction func buttonEarningsTab2Clicked(_ sender: Any) {
        var image = UIImage(named: "home_icon")
        buttonHome.setImage(image, for: .normal)

        image = UIImage(named: "earnings_active_icon")
        buttonEarnings.setImage(image, for: .normal)

        image = UIImage(named: "notifications_icon")
        buttonNotification.setImage(image, for: .normal)

        image = UIImage(named: "settings_icon")
        buttonSettings.setImage(image, for: .normal)

    }
    
    
    @IBAction func buttonNotificationTab3Clicked(_ sender: Any) {
        var image = UIImage(named: "home_icon")
     buttonHome.setImage(image, for: .normal)

     image = UIImage(named: "earnings_icon")
     buttonEarnings.setImage(image, for: .normal)

     image = UIImage(named: "notifications_active_icon")
     buttonNotification.setImage(image, for: .normal)

     image = UIImage(named: "settings_icon")
     buttonSettings.setImage(image, for: .normal)
    }
    
    
    @IBAction func buttonSettingsTab4Clicked(_ sender: Any) {
       var image = UIImage(named: "home_icon")
        buttonHome.setImage(image, for: .normal)

        image = UIImage(named: "earnings_icon")
        buttonEarnings.setImage(image, for: .normal)

        image = UIImage(named: "notifications_icon")
        buttonNotification.setImage(image, for: .normal)

        image = UIImage(named: "settings_active_icon")
        buttonSettings.setImage(image, for: .normal)
    }
    
    
   @IBAction func buttonProfileTab5Clicked(_ sender: Any) {
    
      }
      
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrList.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let separatorLineView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 25))
        /// change size as you need.
        separatorLineView.backgroundColor = UIColor.clear
        // you can also put image here
        return separatorLineView

    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 25
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // create a new cell if needed or reuse an old one
//        let cell:HomeCell = (tableView.dequeueReusableCell(withIdentifier: "HomeCell") as HomeCell?)!?

        let cell:HomeCell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as! HomeCell
        cell.contentView.layer.cornerRadius = 18
        cell.contentView.layer.borderColor = UIColor.green.cgColor.copy(alpha: 0.3)
        cell.contentView.layer.borderWidth = 0.5
        
        cell.selectionStyle = .none
        
        cell.labelTitle.text = arrList[indexPath.section]
        let strImageName = String(format: "ride_icon%d", indexPath.section)
        cell.imageViewRideSelect.image = UIImage(named: strImageName)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bookRideVC = self.storyboard?.instantiateViewController(identifier: "BookRideVC") as! BookRideVC
        self.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(bookRideVC, animated: true)
    }
    



}



extension UIView {

   func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
       layer.masksToBounds = false
       layer.shadowColor = color.cgColor
       layer.shadowOpacity = opacity
       layer.shadowOffset = offSet
       layer.shadowRadius = radius

       layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
       layer.shouldRasterize = true
       layer.rasterizationScale = scale ? UIScreen.main.scale : 1
     }
}
