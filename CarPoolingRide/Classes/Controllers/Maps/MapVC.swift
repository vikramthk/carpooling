//
//  MapVC.swift
//  CarPoolingRide
//
//  Created by user on 11/01/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit

class MapVC: BaseViewController {

    @IBOutlet weak var viewPopin: UIView!
    var mapView:GMSMapView?
    
    @IBOutlet weak var buttonStartRide: UIButton!
    @IBOutlet weak var buttonTrack: UIButton!
    @IBOutlet weak var buttonComplete: UIButton!
    @IBOutlet weak var buttonVerify: UIButton!
    
    @IBOutlet weak var viewVerifyNumber: UIView!
    @IBOutlet weak var viewCongo: UIView!
    @IBOutlet weak var viewComplete: UIView!
    
    @IBOutlet weak var buttonDriverArrived: UIButton!
    @IBOutlet weak var mapview: MKMapView!
    
    @IBOutlet var layoutConstraintMapHeight: [NSLayoutConstraint]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.view.backgroundColor = UIColor.red
        viewVerifyNumber.isHidden = true
        viewPopin.isHidden = false
        viewCongo.isHidden = true
        viewComplete.isHidden = true
        setCustomValues()
        
 
        let image = UIImage(named: "back")?.withRenderingMode(.alwaysTemplate)
        self.buttonBack.setImage(image, for: .normal)
        self.buttonBack.tintColor = UIColor.black
        self.view.endEditing(true)
    }
    
    func setCustomValues(){
        buttonStartRide.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        buttonComplete.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        buttonTrack.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        buttonDriverArrived.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        buttonVerify.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        viewVerifyNumber.layer.cornerRadius = 8.0
        viewCongo.layer.cornerRadius = 8.0

        buttonTrack.layer.borderColor = UIColor.gray.cgColor;
        buttonTrack.layer.borderWidth = 1.0
        
        DispatchQueue.main.async {
        self.view.endEditing(true)
        }
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
   
    @IBAction func buttonDriverArrivedClicked(_ sender: Any) {
        viewVerifyNumber.isHidden =  false
        viewPopin.isHidden = true

    }
    
    @IBAction func buttonArrive(_ sender: Any) {
        viewVerifyNumber.isHidden =  false
        viewPopin.isHidden = true

    }
    @IBAction func b(_ sender: Any) {
        viewVerifyNumber.isHidden =  false
        viewPopin.isHidden = true

    }
    @IBAction func buttonVerifyClicked(_ sender: Any) {
        viewVerifyNumber.isHidden =  true
        viewCongo.isHidden = false
    }
    
    @IBAction func buttonStartClicked(_ sender: Any) {
        viewCongo.isHidden = true
        viewComplete.isHidden = false

       }
    
    @IBAction func buttonTrackClicked(_ sender: Any) {
        
    }
    
    @IBAction func buttonCompleteClicked(_ sender: Any) {
        viewComplete.isHidden = true
        self.pushViewController("CompletedVC")
    }
    
    
    //    override func loadView() {
//         Create a GMSCameraPosition that tells the map to display the
    //         coordinate -33.86,151.20 at zoom level 6.
      //  let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        // mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        //viewPopin = mapView
        //viewPopin.addSubview(mapView)
        // Creates a marker in the center of the map.
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//        marker.title = "Sydney"
//        marker.snippet = "Australia"
//        marker.map = mapView
//
//        mapView = GMSMapView.map(withFrame: CGRect(x: 100, y: 100, width: 200, height: 200), camera: GMSCameraPosition.camera(withLatitude: 51.050657, longitude: 10.649514, zoom: 5.5))

//        so; the mapView is of width 200, height 200 and its center is same as center of the self.view
        //mapView!.center = self.viewPopin.center

       // let map = MKMapView(frame: CGRect(x: 100, y: 100, width: 200, height: 200))
        //self.view.addSubview(map)
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
