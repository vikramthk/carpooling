//
//  CardViewController.swift
//  CarPoolingRide
//
//  Created by Abhishek Patel on 15/02/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit

class CardViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var tableViewList: UITableView!
    
//    var arrList = ["Credit Card","Debit Card" ,"Add New Card"]
//    var arrListImages = ["credtcard","credtcard" ,"addcard"]
    var arrList = ["Credit Card","Debit Card" ]
    var arrListImages = ["credtcard","credtcard"]

    @IBOutlet weak var buttonPay: UIButton!
    var nExpandIndex = -1
    var nTempIndex = -1
    var nFooterHeight = 0
    var isClicked = false
    var myCustomView: CardView = CardView()
    
    @IBOutlet weak var buttonAbuse: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonAbuse.layer.cornerRadius = 6
        myCustomView  = UIView.fromNib()
        buttonPay.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
    }
    
    @IBAction func buttonAbuseClicked(_ sender: Any) {
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 75
     }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return arrList.count
        }

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            myCustomView  = UIView.fromNib()

            if section == 0 {
                           myCustomView.textFieldCreditCard.placeholder = "Credit Card"
                       }else if section == 1 {
                           myCustomView.textFieldCreditCard.placeholder = "Debit Card"
                       }
             let separatorLineView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 25))
            /// change size as you need.
            separatorLineView.backgroundColor = UIColor.red
            // you can also put image here
            return myCustomView
         }
    
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
  
            if isClicked == false {
                return 0
            }
            
            if nExpandIndex == section  {
                 nTempIndex = nExpandIndex
                 return  CGFloat(nFooterHeight)
            }
            return CGFloat(0)
        }
        
        // create a cell for each table view row1z
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            // create a new cell if needed or reuse an old one
    //     let cell:HomeCell = (tableView.dequeueReusableCell(withIdentifier: "HomeCell") as HomeCell?)!?

            let cell:CardCell = tableView.dequeueReusableCell(withIdentifier: "CardCell") as! CardCell
//            cell.contentView.layer.cornerRadius = 18
//            cell.contentView.layer.borderColor = UIColor.lightGray.cgColor.copy(alpha: 1)
//            cell.contentView.layer.borderWidth = 0.5
//
            cell.selectionStyle = .none
            cell.labelCardName.text = arrList[indexPath.section]

    //        cell.labelTitle.text = arrList[indexPath.section]
    //        let strImageName = String(format: "ride_icon%d", indexPath.section)
    //        cell.imageViewRideSelect.image = UIImage(named: strImageName)
          //  cell.imageViewProfile.layer.cornerRadius = cell.imageViewProfile.frame.size.width/2
            //cell.buttonRequest.layer.cornerRadius = 6.0
            if nExpandIndex == indexPath.section && isClicked == true{
             
                cell.imageViewUpDown.image = UIImage(named: "up_arrow")
            }else{
                cell.imageViewUpDown.image = UIImage(named: "down_arrow")
            }
            return cell
        }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nFooterHeight = Int(220.0)
        if isClicked == false {
            isClicked = true
        }else{
            isClicked = false
        }
        nExpandIndex = indexPath.section
        tableView.reloadData()
    }


}



extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
