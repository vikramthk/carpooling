//
//  CompletedVC.swift
//  CarPoolingRide
//
//  Created by user on 15/01/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit

class CompletedVC: BaseViewController {

    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var buttonPay: UIButton!
    @IBOutlet weak var buttonAbuse: UIButton!
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var viewTotal: UIView!
    override func viewDidLoad() {
      super.viewDidLoad()
      imageViewProfile.layer.cornerRadius = imageViewProfile.frame.size.width/2
        viewTotal.layer.borderWidth = 1.0
        viewTotal.layer.borderColor = UIColor.gray.cgColor
        buttonPay.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        buttonAbuse.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        scrollview.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
      }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
    }
    @IBAction func buttonAbuseClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "PaymentVC") as! PaymentVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func buttonPayClicked(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
