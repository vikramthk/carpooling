//
//  ProfileCell.swift
//  CarPoolingRide
//
//  Created by user on 31/12/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelRate: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var viewRate: UIView!
    
    @IBOutlet weak var viewButtons: UIView!
    @IBOutlet weak var buttonReview: UIButton!
    @IBOutlet weak var buttonCarDetails: UIButton!
    @IBOutlet weak var viewCustomerReviews: UIView!
    @IBOutlet weak var viewCarDetails: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewRate.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
