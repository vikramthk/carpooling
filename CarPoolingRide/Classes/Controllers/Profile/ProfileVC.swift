//
//  HomeVC.swift
//  CarPoolingRide
//
//  Created by user on 29/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit
//import GoogleMaps


class ProfileVC: BaseViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var viewRate: UIView!
    @IBOutlet weak var labelRate: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var imageViewProfile: UIImageView!
    
    @IBOutlet weak var buttonCheck: UIButton!
    @IBOutlet weak var buttonCall: UIButton!

    @IBOutlet weak var viewOptions: UIView!
    @IBOutlet weak var collectionViewPef: UICollectionView!
    @IBOutlet weak var viewList: UIView!
    @IBOutlet weak var viewHeader: UIView!
    
    var isReviewSelected : Bool!
    var arrList = ["Search Ride","Offer Ride","Current Ride","Ride History"]
    var arrListPref = ["No Smoke","No Weapons"]
    var arrListImagesPref = ["no_smoke.png","no_weapons.png"]

//    override func loadView() {
//      // Create a GMSCameraPosition that tells the map to display the
//      // coordinate -33.86,151.20 at zoom level 6.
//      let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
//      let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//      view = mapView
//
//      // Creates a marker in the center of the map.
//      let marker = GMSMarker()
//      marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//      marker.title = "Sydney"
//      marker.snippet = "Australia"
//      marker.map = mapView
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isReviewSelected = true
        viewOptions.layer.cornerRadius = 8.0
        viewHeader.layer.cornerRadius = 20.0
        self.viewList.layer.cornerRadius = 32.0
        self.setCornerRadius()
     }
    
    func setCornerRadius(){
        self.viewRate.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        self.buttonCheck.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        self.buttonCall.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        self.buttonCall.layer.borderWidth = 1.0
        self.buttonCall.layer.borderColor = Constants.kColorThemeGreen.cgColor
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
 
    }
    
    @IBAction func buttonReviewClicked(_ sender: Any) {
        isReviewSelected = true
        tableViewList.reloadData()
    }
    
    @IBAction func buttonCarDetailsClicked(_ sender: Any) {
        isReviewSelected = false
        tableViewList.reloadData()
    }
    
    @IBAction func buttonCheckClicked(_ sender: Any) {
    }
    
    @IBAction func buttonCallClicked(_ sender: Any) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 77
        }
        return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let separatorLineView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 25))
        /// change size as you need.
        separatorLineView.backgroundColor = UIColor.clear
        // you can also put image here
        return separatorLineView

    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // create a new cell if needed or reuse an old one
//        let cell:HomeCell = (tableView.dequeueReusableCell(withIdentifier: "HomeCell") as HomeCell?)!?

        let cell:ProfileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
 
        cell.selectionStyle = .none
        cell.imageViewProfile.layer.cornerRadius = cell.imageViewProfile.frame.size.width/2
        if indexPath.row == 0 {
            cell.viewButtons.isHidden = false
            cell.imageViewProfile.isHidden = true
            cell.labelDesc.isHidden = true
            cell.labelName.isHidden = true
            cell.viewRate.isHidden = true
            cell.buttonReview.addTarget(self, action: #selector(buttonReviewClicked(_:)), for: .touchUpInside)
            cell.buttonCarDetails.addTarget(self, action: #selector(buttonCarDetailsClicked(_:)), for: .touchUpInside)
            if isReviewSelected == true{
                cell.viewCustomerReviews.backgroundColor = Constants.kColorThemeGreen
                cell.viewCarDetails.backgroundColor = UIColor.gray
                cell.buttonReview.setTitleColor(Constants.kColorThemeGreen, for: .normal)
                cell.buttonCarDetails.setTitleColor(UIColor.gray, for: .normal)
            }else{
                cell.viewCustomerReviews.backgroundColor = UIColor.gray
                cell.viewCarDetails.backgroundColor = Constants.kColorThemeGreen
                cell.buttonReview.setTitleColor(UIColor.gray, for: .normal)
                cell.buttonCarDetails.setTitleColor(Constants.kColorThemeGreen, for: .normal)
            }
        }else{
            cell.viewButtons.isHidden = true
            cell.imageViewProfile.isHidden = false
            cell.labelDesc.isHidden = false
            cell.labelName.isHidden = false
            cell.viewRate.isHidden = false
        }
//        cell.labelTitle.text = arrList[indexPath.section]
//        let strImageName = String(format: "ride_icon%d", indexPath.section)
//        cell.imageViewRideSelect.image = UIImage(named: strImageName)
        return cell
    }
    



    // tell the collection view how many cells to make
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrListPref.count
       }

       // make a cell for each cell index path
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

           // get a reference to our storyboard cell
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreferenceCell", for: indexPath as IndexPath) as! PreferenceCell

        cell.labelPref.text = arrListPref[indexPath.row]
        cell.imageViewPref.image = UIImage(named: arrListImagesPref[indexPath.row])
        
           return cell
       }

       // MARK: - UICollectionViewDelegate protocol

       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       }
}



extension UIView {

 
}
