//
//  PreferenceCell.swift
//  CarPoolingRide
//
//  Created by user on 30/12/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

class PreferenceCell: UICollectionViewCell {
    @IBOutlet weak var imageViewPref: UIImageView!
    @IBOutlet weak var labelPref: UILabel!
}
