//
//  FeedbackVC.swift
//  CarPoolingRide
//
//  Created by Abhishek Patel on 12/02/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit

class FeedbackVC: BaseViewController {

    @IBOutlet weak var buttonReport: UIButton!
    @IBOutlet weak var textViewFeedback: UITextView!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
  //  @IBOutlet weak var scrollview: UIScrollView!

    @IBOutlet weak var sliderFeedback: UISlider!
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewFeedback.layer.borderWidth = 1.0
        textViewFeedback.layer.borderColor = UIColor.lightGray.cgColor
        buttonReport.layer.cornerRadius = CGFloat(2)
        buttonSubmit.layer.cornerRadius = CGFloat(Float(Constants.kButtonRadius))
        textViewFeedback.layer.cornerRadius = CGFloat(Float(Constants.kButtonRadius))
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        //scrollview.contentSize = CGSize(width: self.view.frame.size.width, height: 500)
    }
    
    @IBAction func buttonAbuseClicked(_ sender: Any) {
    }
    @IBAction func buttonSubmitClicked(_ sender: Any) {
    }
    
    @IBAction func sliderChanged(_ sender: Any) {
        if(sliderFeedback.value < 20){
            activeSmiley1()
        }else  if(sliderFeedback.value > 20 && sliderFeedback.value < 40){
            activeSmiley2()
        }else  if(sliderFeedback.value > 40 && sliderFeedback.value < 60){
            activeSmiley3()
        }else  if(sliderFeedback.value > 60 && sliderFeedback.value < 80){
            activeSmiley4()
        }else  if(sliderFeedback.value > 80 && sliderFeedback.value <= 100){
            activeSmiley5()
        }
        
    }
    
    func activeSmiley1(){
        img1.image = UIImage(named: "smiley1_active")
        img2.image = UIImage(named: "smiley2")
        img3.image = UIImage(named: "smiley3")
        img4.image = UIImage(named: "smiley4")
        img5.image = UIImage(named: "smiley5")
    }
    
    func activeSmiley2(){
        img1.image = UIImage(named: "smiley1")
        img2.image = UIImage(named: "smiley2_active")
        img3.image = UIImage(named: "smiley3")
        img4.image = UIImage(named: "smiley4")
        img5.image = UIImage(named: "smiley5")
 
    }

    
    func activeSmiley3(){
        img1.image = UIImage(named: "smiley1")
        img2.image = UIImage(named: "smiley2")
        img3.image = UIImage(named: "smiley3_active")
        img4.image = UIImage(named: "smiley4")
        img5.image = UIImage(named: "smiley5")
 

    }

    
    func activeSmiley4(){
        img1.image = UIImage(named: "smiley1")
        img2.image = UIImage(named: "smiley2")
        img3.image = UIImage(named: "smiley3")
        img4.image = UIImage(named: "smiley4_active")
        img5.image = UIImage(named: "smiley5")
    }

    
    func activeSmiley5(){
        img1.image = UIImage(named: "smiley1")
        img2.image = UIImage(named: "smiley2")
        img3.image = UIImage(named: "smiley3")
        img4.image = UIImage(named: "smiley4")
        img5.image = UIImage(named: "smiley5_active")

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
