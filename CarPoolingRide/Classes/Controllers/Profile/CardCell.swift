//
//  RequestedRideCell.swift
//  CarPoolingRide
//
//  Created by user on 05/01/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {
 
    @IBOutlet weak var labelCardName: UILabel!
    @IBOutlet weak var imageViewCard: UIImageView!
    @IBOutlet weak var imageViewUpDown: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
     }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
