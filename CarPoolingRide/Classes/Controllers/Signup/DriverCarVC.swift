//
//  DriverCarVC.swift
//  CarPoolingRide
//
//  Created by user on 17/11/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

class DriverCarVC: BaseViewController, UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var viewCarNumberPlate: UIView!
    @IBOutlet weak var viewCarType: UIView!
    @IBOutlet weak var viewCarModel: UIView!
    @IBOutlet weak var viewCarYear: UIView!
    @IBOutlet weak var viewCarColor: UIView!

    @IBOutlet weak var textFieldCarNumberPlate: UITextField!
    @IBOutlet weak var textFieldCarType: UITextField!
    @IBOutlet weak var textFieldCarModel: UITextField!
    @IBOutlet weak var textFieldCarYear: UITextField!
    @IBOutlet weak var textFieldCarColor: UITextField!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonCamera: UIButton!

    @IBOutlet weak var pickerview: UIPickerView!
 
    var picker:UIImagePickerController?=UIImagePickerController()

    var arrCarType = [String]()
    var arrCarModel = [String]()
    var arrCarYear = [String]()
    
    var nSelectedOption = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCutomValues()
    }
    
    func setCutomValues() {
   self.setBorderForTextField(viewCarNumberPlate);
    self.setBorderForTextField(viewCarType);
    self.setBorderForTextField(viewCarModel);
    self.setBorderForTextField(viewCarYear);
    self.setBorderForTextField(viewCarColor);
        buttonNext.layer.cornerRadius = 12.0
     arrCarType = ["Petrol","Gas","Diesel"]
     arrCarModel = ["Petrol1","Gas2","Diesel3"]
     arrCarYear = ["1999","2017","2012"]
        viewBlur.isHidden = true

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        pickerview.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        pickerview.layer.borderColor = Constants.kColorThemeGreen.cgColor
        pickerview.layer.borderWidth = 1.0
     }

    @IBAction func buttonCarTypeClicked(_ sender: Any) {
        viewBlur.isHidden = false
        pickerview.isHidden = false
        nSelectedOption = 0
        pickerview.reloadAllComponents()
    }

    @IBAction func buttonCarModelClicked(_ sender: Any) {
        pickerview.isHidden = false
        viewBlur.isHidden = false

        nSelectedOption = 1
        pickerview.reloadAllComponents()
    }

    @IBAction func buttonCarYearClicked(_ sender: Any) {
        pickerview.isHidden = false
        viewBlur.isHidden = false

        nSelectedOption = 2
        pickerview.reloadAllComponents()
    }

    @IBAction func buttonCameraClicked(_ sender: Any) {
        openGallary()
    }
    @IBAction func buttonNextClicked(_ sender: Any) {
    
    }
    
    @IBAction func buttonSkipClicked(_ sender: Any) {
    }
  
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        pickerview.isHidden = true
        viewBlur.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if nSelectedOption == 0{
            textFieldCarType.text = arrCarType[row]
        }else if nSelectedOption == 1{
            textFieldCarModel.text = arrCarModel[row]
        }else if nSelectedOption == 2{
            textFieldCarYear.text = arrCarYear[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if nSelectedOption == 0{
            return arrCarType[row]
        }else if nSelectedOption == 1{
            return arrCarModel[row]
        }else if nSelectedOption == 2{
            return arrCarYear[row]
        }
        return ""
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
          return 1
      }
      
      func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
      {
          if nSelectedOption == 0{
            return arrCarType.count
                }else if nSelectedOption == 1{
                    return arrCarModel.count
                }else {
                    return arrCarYear.count
                }
      }
    
    func openGallary()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker?.delegate = self
        present(picker!, animated: true, completion: nil)
    }

          
          func openCamera()
          {
              if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                  picker!.allowsEditing = false
                  picker!.sourceType = UIImagePickerController.SourceType.camera
                  picker!.cameraCaptureMode = .photo
                  present(picker!, animated: true, completion: nil)
              }else{
                  let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
                  let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
                  alert.addAction(ok)
                  present(alert, animated: true, completion: nil)
              }
          }
          
          
          func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
              dismiss(animated: true, completion: nil)
          }
      
          func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
              if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                  //self.imageViewPicture.image = image
                buttonCamera.setImage(image, for: .normal)
             } else{
                print("Something went wrong in  image")
            }
            dismiss(animated: true, completion: nil)

              
      }
      
}
