//
//  SignupVC.swift
//  CarPoolingRide
//
//  Created by user on 28/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit


class VerifyMobileVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var pinView: JAPinView!

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var textFieldMobile: UITextField!
    @IBOutlet weak var buttonNext: UIButton!

    @IBOutlet weak var labelTimer: UILabel!
    var nCounter = 10;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCutomValues()
    }
    
    func setCutomValues() {
//        viewMain.layer.cornerRadius = 12.0;
//        viewMain.layer.borderColor = UIColor.gray.cgColor;
//        viewMain.layer.borderWidth = 1.0
        buttonNext.layer.cornerRadius = 12.0;
        buttonNext.layer.masksToBounds = true
        
        labelTimer.text = String(format: "00:%d", nCounter)
        pinView.setFont(UIFont.systemFont(ofSize: 25))
        pinView.onSuccessCodeEnter = { pin in
        print(pin)
        }
        
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setBorderForTextField(_ view : UIView){
        view.layer.cornerRadius = 12.0;
        view.layer.borderColor = UIColor.gray.cgColor;
        view.layer.borderWidth = 1.0
    }

    @objc func callTimer(){
        if nCounter >= 0 {
            labelTimer.text = String(format: "00:0%d", nCounter)
            nCounter = nCounter - 1
            self.perform(#selector(callTimer), with: nil, afterDelay: 1)
        }
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
        self.perform(#selector(callTimer), with: nil, afterDelay: 0)

    }
    
    @IBAction func buttonSkipClicked(_ sender: Any) {
        
    }


}
