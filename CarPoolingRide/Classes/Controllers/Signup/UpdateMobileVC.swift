//
//  SignupVC.swift
//  CarPoolingRide
//
//  Created by user on 28/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit
import MRCountryPicker


class UpdateMobileVC: UIViewController, UITextFieldDelegate,MRCountryPickerDelegate {

    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var textFieldMobile: UITextField!
    @IBOutlet weak var buttonNext: UIButton!

    
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var labelCountry: UILabel!
    @IBOutlet weak var imageViewFlag: UIImageView!
    @IBOutlet weak var viewBlur: UIView!

    var strSelectedCountry = ""
    var strSelectedCountryCode = ""
    var imageFlag : UIImage = UIImage()
      

    override func viewDidLoad() {
        super.viewDidLoad()
        setCutomValues()
     }
    
    func setCutomValues(){
        self.setBorderForTextField(viewMobile);
        buttonNext.layer.cornerRadius = 12.0;

        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPicker.setCountry("SI")
        countryPicker.setLocale("sl_SI")
        countryPicker.setCountryByName("United States")
        showHidePicker(true)
        countryPicker.layer.cornerRadius  = CGFloat(Constants.kButtonRadius)
        countryPicker.layer.borderWidth = 1.0
        countryPicker.layer.borderColor = Constants.kColorThemeGreen.cgColor

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        countryPicker.isHidden = true
        viewBlur.isHidden = true
    }
    
    func showHidePicker(_ isHidden: Bool){
        countryPicker.isHidden = isHidden
        viewBlur.isHidden = isHidden
    }
    
    func setBorderForTextField(_ view : UIView){
        view.layer.cornerRadius = 12.0;
        view.layer.borderColor = UIColor.gray.cgColor;
        view.layer.borderWidth = 1.0
    }
    
    @IBAction func buttonCountryClicked(_ sender: Any) {
        showHidePicker(false)
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
        
    }
    
    @IBAction func buttonSkipClicked(_ sender: Any) {
        
    }
    
    
    @IBAction func buttonCancelClicked(_ sender: Any) {
        showHidePicker(true)
    }

    @IBAction func buttonDoneClicked(_ sender: Any) {
        showHidePicker(true)
        imageViewFlag.image = imageFlag
        labelCountry.text = strSelectedCountry
    }

    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        strSelectedCountry = name
        strSelectedCountryCode = countryCode
        imageFlag = flag
        imageViewFlag.image = imageFlag
        labelCountry.text = strSelectedCountry

    }
    
  

}
