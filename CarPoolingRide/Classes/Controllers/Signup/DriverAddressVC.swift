//
//  DriverAddressVC.swift
//  CarPoolingRide
//
//  Created by user on 17/11/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit
import MRCountryPicker

class DriverAddressVC: BaseViewController,MRCountryPickerDelegate,UITextFieldDelegate {

    @IBOutlet weak var countryPicker: MRCountryPicker!

    @IBOutlet weak var textFieldAddress: UITextField!
    @IBOutlet weak var textFieldCity: UITextField!
    @IBOutlet weak var textFieldZip: UITextField!
    @IBOutlet weak var textFieldState: UITextField!
    @IBOutlet weak var textFieldCountry: UITextField!

    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewZip: UIView!
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var viewState: UIView!
    var strSelectedCountry = ""
    var strSelectedCountryCode = ""
    var imageFlag : UIImage = UIImage()
 
     @IBOutlet weak var buttonNext: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        countryPicker.isHidden = true

        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPicker.setCountry("SI")
        countryPicker.setLocale("sl_SI")
        countryPicker.setCountryByName("United States")
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        self.setBorderForTextField(viewAddress);
        self.setBorderForTextField(viewCity);
        self.setBorderForTextField(viewZip);
        self.setBorderForTextField(viewCountry);
        self.setBorderForTextField(viewState);

        buttonNext.layer.cornerRadius = 12.0
        countryPicker.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        countryPicker.layer.borderWidth = 1.0
        countryPicker.layer.borderColor = Constants.kColorThemeGreen.cgColor

 
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        countryPicker.isHidden = true
        viewBlur.isHidden = true
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
           //self.pushViewController("DriverCarVC")
        let tabBarController = self.storyboard?.instantiateViewController(identifier: "MainTabBarController")  as! MainTabBarController
        UIApplication.shared.windows.first?.rootViewController = tabBarController
        }
       
       @IBAction func buttonSkipClicked(_ sender: Any) {
           //self.pushViewController("DriverCarVC")
       }
    
    @IBAction func buttonCountryclicked(_ sender: Any) {
        textFieldAddress.resignFirstResponder()
        textFieldCity.resignFirstResponder()
        textFieldZip.resignFirstResponder()
        textFieldState.resignFirstResponder()
        countryPicker.isHidden = false
        viewBlur.isHidden = false
    }
   

    func textFieldDidBeginEditing(_ textField: UITextField) {
        countryPicker.isHidden = true
    }
    

    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
          strSelectedCountry = name
          strSelectedCountryCode = countryCode
          imageFlag = flag
          textFieldCountry.text = strSelectedCountry
      }

}
