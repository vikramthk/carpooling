//
//  SignupVC.swift
//  CarPoolingRide
//
//  Created by user on 28/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit


class VeryfyVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var pinView: JAPinView!

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var textFieldMobile: UITextField!
    @IBOutlet weak var buttonNext: UIButton!

  
    override func viewDidLoad() {
        super.viewDidLoad()
        setCutomValues()
        // Do any additional setup after loading the view.
    }
    
    func setCutomValues() {
//        viewMain.layer.cornerRadius = 12.0;
//        viewMain.layer.borderColor = UIColor.gray.cgColor;
//        viewMain.layer.borderWidth = 1.0
        buttonNext.layer.cornerRadius = 12.0;
        buttonNext.layer.masksToBounds = true

        pinView.setFont(UIFont.systemFont(ofSize: 25))
            pinView.onSuccessCodeEnter = { pin in
                           print(pin)
        }
    }
    

    
    func setBorderForTextField(_ view : UIView){
        view.layer.cornerRadius = 12.0;
        view.layer.borderColor = UIColor.gray.cgColor;
        view.layer.borderWidth = 1.0
    }

    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
    }
    
    @IBAction func buttonSkipClicked(_ sender: Any) {
        
    }
    @IBAction func buttonResendClicked(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "VerifyMobileVC") as! VerifyMobileVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
