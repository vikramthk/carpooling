//
//  SignupVC.swift
//  CarPoolingRide
//
//  Created by user on 28/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit
 import MRCountryPicker
import PhoneNumberKit


class  LoginVC: BaseViewController, UITextFieldDelegate,MRCountryPickerDelegate {
 
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        strSelectedCountry = name
        strSelectedCountryCode = countryCode
        imageFlag = flag
        imageViewFlag.image = imageFlag
        labelCountry.text = strSelectedCountry
    }

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewCountry: UIView!
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var textFieldMobile: PhoneNumberTextField!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonNextEmail: GreenButton!
    @IBOutlet weak var viewEmailAddress: UIView!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var labelCountry: UILabel!
    @IBOutlet weak var imageViewFlag: UIImageView!
    
    @IBOutlet weak var countryPicker: MRCountryPicker!
 
    var strSelectedCountry = ""
    var strSelectedCountryCode = ""
    var imageFlag : UIImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCutomValues()
    }
    
    func setCutomValues() {
        //viewMain.layer.cornerRadius = 12.0;
        // viewMain.layer.borderColor = UIColor.gray.cgColor;
       // viewMain.layer.borderWidth = 1.0
        self.setBorderForTextField(viewMobile);
       // self.setBorderForTextField(viewEmail);
        self.setBorderForTextField(viewCountry);
        self.setBorderForTextField(viewEmailAddress);
        buttonNext.layer.cornerRadius = 12.0;
        buttonNextEmail.layer.cornerRadius = 12.0;
        
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPicker.setCountry("SI")
        countryPicker.setLocale("sl_SI")
        countryPicker.setCountryByName("United States")
        showHidePicker(true)
        
        textFieldEmail.delegate = self
        textFieldMobile.delegate = self
        buttonBack.isHidden = true
        countryPicker.layer.cornerRadius = CGFloat(Constants.kButtonRadius)
        countryPicker.layer.borderWidth = 1.0
        countryPicker.layer.borderColor = Constants.kColorThemeGreen.cgColor
         let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        self.callLoginWebService()
    }

    func callLoginWebService(){
        var dict = ["email":"johndoe@ride.com","password":"123456","method":"login"] as NSMutableDictionary
        ServerRequest.sendServerRequest_POST(dictData: dict) { (res, isSuccess) in
            print("respoooo")
            print(res)
            print(isSuccess)
            
        }
    }
    
      func showHidePicker(_ isHidden: Bool){
        countryPicker.isHidden = isHidden
        viewBlur.isHidden = isHidden
    }
    
    @IBAction func buttonCountryClicked(_ sender: Any) {
           showHidePicker(false)
     }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
        showHidePicker(true)

        let vc = self.storyboard?.instantiateViewController(identifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func buttonNextEmailClicked(_ sender: Any) {
//        if !self.isValidEmail(emailStr: textFieldEmail.text!){
//            self.showAlertWithOk(Constants.kValidEmail);
//        }else{
            let vc = self.storyboard?.instantiateViewController(identifier: "VeryfyVC") as! VeryfyVC
            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        viewEmail.isHidden = true;
        viewMain.isHidden = false;
        buttonBack.isHidden = true

    }
    
    @IBAction func buttonFindClicked(_ sender: Any) {
        showHidePicker(true)
        viewEmail.isHidden = false;
        viewMain.isHidden = true;
        buttonBack.isHidden = false
    }
    
    @IBAction func buttonCancelClicked(_ sender: Any) {
        showHidePicker(true)
    }

    @IBAction func buttonDoneClicked(_ sender: Any) {
        showHidePicker(true)
        imageViewFlag.image = imageFlag
        labelCountry.text = strSelectedCountry
    }
    
    override func buttonBackClicked() {
        viewEmail.isHidden = true;
        viewMain.isHidden = false;
        buttonBack.isHidden = true
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        showHidePicker(true)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if(textField == textFieldMobile){
            let currentCharacterCount = textField.text?.count ?? 0
            if range.length + range.location > currentCharacterCount {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 14
        }else{
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        showHidePicker(true)
     }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
 

}
