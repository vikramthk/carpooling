//
//  DriverDocumentsVC.swift
//  CarPoolingRide
//
//  Created by user on 17/11/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

class DriverDocumentsVC: BaseViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate{
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewLicenceNum: UIView!
    @IBOutlet weak var textFeildLicenceNum: UITextField!
    @IBOutlet weak var viewSSL: UIView!
    @IBOutlet weak var textFieldssl: UITextField!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var imageViewLicencePhoto: UIImageView!
    
    var picker:UIImagePickerController?=UIImagePickerController()

    @IBOutlet weak var layoutConstraintTopspace: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCutomValues()
    }
    
    func setCutomValues() {
        self.setBorderForTextField(viewLicenceNum);
        self.setBorderForTextField(viewSSL);
        buttonNext.layer.cornerRadius = 12.0;
        let modelName = UIDevice.current.name
              print("ttttttttesting....")
              print(modelName)
              if modelName == "iPhone 8" || modelName == "iPhone 8 Plus" {
                layoutConstraintTopspace.constant = 200
             }
    }
    

    func openGallary()
    {
        picker!.delegate = self
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }

    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerController.SourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    

    
    @IBAction func buttonNextClicked(_ sender: Any) {
        self.pushViewController("DriverAddressVC")
    }
    
    @IBAction func buttonSkipClicked(_ sender: Any) {
        self.pushViewController("DriverAddressVC")
    }
    
    @IBAction func buttonDocumentClicked(_ sender: Any) {
        openGallary()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           if textField == textFeildLicenceNum{
               textFieldssl.becomeFirstResponder()
           }else if textField == textFieldssl{
               textFieldssl.resignFirstResponder()
           }
           return true
       }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
    
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                self.imageViewLicencePhoto.image = image
           } else{
              print("Something went wrong in  image")
          }
          dismiss(animated: true, completion: nil)
        }
    
       
}
