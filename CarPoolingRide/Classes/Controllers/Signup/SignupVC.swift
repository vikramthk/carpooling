//
//  SignupVC.swift
//  CarPoolingRide
//
//  Created by user on 28/09/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

class  SignupVC: BaseViewController, UITextFieldDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate {

    //@IBOutlet weak var viewMain: UIView!
    //@IBOutlet weak var viewCountry: UIView!
    //@IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var viewEmail: UIView!
    //@IBOutlet weak var textFieldMobile: UITextField!
    //@IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonNextEmail: UIButton!
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var viewEmailAddress: UIView!
    @IBOutlet weak var textFieldFirstName: UITextField!
    @IBOutlet weak var textFieldLastName: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var imageViewPicture: UIImageView!
    
    var picker:UIImagePickerController?=UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCutomValues()
        // Do any additional setup after loading the view.
    }
    
    func setCutomValues() {
//        viewMain.layer.cornerRadius = 12.0;
//        viewMain.layer.borderColor = UIColor.gray.cgColor;
//        viewMain.layer.borderWidth = 1.0
//        self.setBorderForTextField(viewMobile);
        //self.setBorderForTextField(viewEmail);
  //      self.setBorderForTextField(viewCountry);
        self.setBorderForTextField(viewEmailAddress);
        self.setBorderForTextField(viewFirstName);
        self.setBorderForTextField(viewLastName);
    //    buttonNext.layer.cornerRadius = 12.0;
        buttonNextEmail.layer.cornerRadius = 12.0;
        picker?.delegate=self
        
        imageViewPicture.layer.cornerRadius = imageViewPicture.frame.size.height/2
        imageViewPicture.layer.masksToBounds = true
    }
    
 
    
    @IBAction func buttonCountryClicked(_ sender: Any) {
        
    }
    
    @IBAction func buttonImageClicked(_ sender: Any) {
        openGallary()
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
        self.pushViewController("DriverDocumentsVC")
    }
    
    @IBAction func buttonFindClicked(_ sender: Any) {
    
    }
    
    @IBAction func buttonBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldFirstName{
            textFieldLastName.becomeFirstResponder()
        }else if textField == textFieldLastName{
            textFieldEmail.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    
 
        func openGallary()
        {
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
            present(picker!, animated: true, completion: nil)
        }

        
        func openCamera()
        {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                picker!.allowsEditing = false
                picker!.sourceType = UIImagePickerController.SourceType.camera
                picker!.cameraCaptureMode = .photo
                present(picker!, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
            }
        }
        
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
    
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                self.imageViewPicture.image = image
           } else{
              print("Something went wrong in  image")
          }
          dismiss(animated: true, completion: nil)

            
    }
    

}
