//
//  PopupVC.swift
//  Popup
//
//  Created by Ketan Patel on 25/07/17.
//  Copyright © 2017 Ketan Patel. All rights reserved.
//

import UIKit

class NoRideVC: BaseViewController {

    @IBOutlet private weak var viewPopupUI:UIView!
    @IBOutlet private weak var viewMain:UIView!
    
    @IBOutlet weak var buttonNo: UIButton!
    @IBOutlet weak var buttonYes: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showViewWithAnimation()
        self.setCustomValues()
    }
    
    func setCustomValues(){
        buttonNo.layer.cornerRadius = 8.0
        buttonYes.layer.cornerRadius = 8.0
        viewPopupUI.layer.cornerRadius = 14.0
        self.setBorderForTextField(buttonNo)
    }
   
    @IBAction private func btnGoOfflinePressed(btnSender: UIButton) {
        self.hideViewWithAnimation()
    }
    
    @IBAction private func btnGoDrivingPressed(btnSender: UIButton) {
        self.hideViewWithAnimation()
    }
 
    //MARK: - Animation Method
    
    private func showViewWithAnimation() {
        
        //self.view.alpha = 0
        //self.viewPopupUI.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
       // UIView.animate(withDuration: 0.3) {
            self.viewPopupUI.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.view.alpha = 1
        //}
        
    }
    
    private func hideViewWithAnimation() {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPopupUI.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.view.alpha = 0
            
        }, completion: {
            (value: Bool) in
            
            self.removeFromParent()
            self.view.removeFromSuperview()
        })
    }

}
