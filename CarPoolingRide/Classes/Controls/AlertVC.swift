//
//  AlertVC.swift
//  CarPoolingRide
//
//  Created by user on 22/12/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

class AlertVC: UIViewController {

    @IBOutlet weak var buttonTry: UIButton!
    
    @IBOutlet weak var viewSorry: UIView!
    @IBOutlet weak var buttonSave: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonTry.layer.cornerRadius = 6.0
        buttonSave.layer.cornerRadius = 6.0
        buttonTry.layer.borderColor = UIColor.gray.cgColor
        buttonTry.layer.borderWidth = 1.0
          viewSorry.layer.cornerRadius = 12.0

        // Do any additional setup after loading the view.
    }
    

    @IBAction func buttnTryClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }

    @IBAction func buttnSaveClicked(_ sender: Any) {

        let vc = self.storyboard?.instantiateViewController(identifier: "RideSummaryVC") as! RideSummaryVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
