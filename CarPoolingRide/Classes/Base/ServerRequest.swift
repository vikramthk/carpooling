//
//  ServerRequest.swift
//  BrewChime
//
//  Created by Nadim on 4/19/18.
//  Copyright © 2018 Vikram. All rights reserved.
//

import UIKit
import Alamofire
 import SwiftyJSON
import ZVProgressHUD

let kMsgCancelled = "cancelled"
let kMsgTimeout = "The request timed out."
let kMsgConnect = "Could not connect to the server."
let kMsgNWLost = "The network connection was lost."

class ServerRequest: NSObject {

  
    
//    func downloadFileFromURL(url: NSURL,completionHandler: CompletionHandler) {

    class func sendServerRequest_POST(dictData : NSMutableDictionary, completion:@escaping(_ response : URLResponse?, _ isSuccess : Bool)->())
    {
        let strMethod = dictData ["method"] as! String
        let strURL = Constants.kServerURL + strMethod
        
        dictData.removeObject(forKey: "method")
      
        let dict = dictData as! Dictionary<String,Any>
        
        print(strURL) 
        print(dictData)
    
         
        var headers: HTTPHeaders = [
            .accept("application/x-www-form-urlencoded")
        ]

            if(strMethod == "login")
            {
             }
        
      AF.request(strURL, method: .post, parameters: dict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print("RESPONSE ")
            print(response)
                                switch response.result{
                                case .success(let res):
                                    ZVProgressHUD.dismiss()
                                   // completion( (res as AnyObject).result! as! URLResponse, true)
                                    
                                    break
                                case .failure(let error):
                                    print("Error from serverRequest")
                                    print(error.localizedDescription)
                                    if strMethod == Constants.kMethodLogin
                                    {
                                        if error.localizedDescription == kMsgTimeout || error.localizedDescription == kMsgConnect || error.localizedDescription ==  kMsgNWLost || error.localizedDescription == kMsgCancelled
                                        {
                                          
                                        }
                                    }
                                      ZVProgressHUD.dismiss()
                }
        }
        
        
        /*
         // if Constants.APP.nIsInterentAvailable  == 1
          //      {
                manager.request(strURL, method: .post, parameters: dict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                    print()
                    print(response)
                    switch response.result{
                    case .success(let res):
                        ZVProgressHUD.dismiss()
                        completion(response, true)
                        break
                    case .failure(let error):
                        print("Error from serverRequest")
                        print(error.localizedDescription)
                        if strMethod == Constants.kMethodLogin
                        {
                            if error.localizedDescription == kMsgTimeout || error.localizedDescription == kMsgConnect || error.localizedDescription ==  kMsgNWLost || error.localizedDescription == kMsgCancelled
                            {
                                var msg =  error.localizedDescription
                                if msg == kMsgCancelled {
                                    msg = kMsgConnect
                                }
                                let alert = UIAlertController(title: Constants.kAPPName, message:msg, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
                                    
//                                    if Constants.APP.isDeviceConnected == 1{
//                                        Constants.APP.isDeviceConnected  = 1
//                                        Constants.APP.isConnectingFinished  = true
//                                     }
                                        completion(response, false)
                                    Constants.APP.setTwoWaySiderAsRootController()
                                    return
                                })
                               Constants.APP.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            }
                        }
                        
                        if error.localizedDescription == kMsgCancelled {
                            
                                self.displayAlertWithOk(message: "Could not connect to the server.")
                        }
                        else{
                           if error.localizedDescription == kMsgTimeout {
                              //  self.displayAlertWithOk(message: error.localizedDescription)
                                let alert = UIAlertController(title: Constants.kAPPName, message:error.localizedDescription, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
//                                    if Constants.APP.isDeviceConnected == 1{
//                                        Constants.APP.isDeviceConnected  = 1
//                                        Constants.APP.isConnectingFinished  = false
//                                     }
                                    completion(response, false)
                                    
                                    if Constants.APP.window?.rootViewController != BleStatusViewController() {
                                        Constants.APP.setTwoWaySiderAsRootController()
                                    }
                                    // completion(response, false)
                                    return
                                })
                                Constants.APP.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            }else{
                                if error.localizedDescription.range(of: kMsgConnect) != nil{
                                    self.displayAlertWithOk(message: error.localizedDescription)
                                }else{
                                    let alert = UIAlertController(title: Constants.kAPPName, message:kMsgConnect, preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
//                                        if Constants.APP.isDeviceConnected == 1{
//                                            Constants.APP.isDeviceConnected  = 1
//                                            Constants.APP.isConnectingFinished  = true
//                                        }
                                            completion(response, false)
                                        if strMethod == Constants.kMethodLogin{
                                            Constants.APP.setTwoWaySiderAsRootController()
                                        }
                                        return
                                    })
                                   Constants.APP.window?.rootViewController?.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                        ZVProgressHUD.dismiss()
                        
                        completion(response, false)
                        break
                    }
                }
//            }else{
//                ZVProgressHUD.dismiss()
//                if strMethod == Constants.kMethodLogin
//                {
//                    let alert = UIAlertController(title: Constants.kAPPName, message:"Could not connect to the server.", preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
//                        Constants.APP.setTwoWaySiderAsRootController()
//                        return
//                    })
//                    Constants.APP.window?.rootViewController?.present(alert, animated: true, completion: nil)
//                }
//               // self.displayAlertWithOk(message: "No Internet Connection")
//            }
 
 */
        
    }
    
 
    
    //-----------------------------------------------------------------------------------------------------------------
    /*
    class func sendServerRequest_GET(_ strMethod : String, completion:@escaping(_ response : DataResponse<Any>, _ isSuccess : Bool)->())
    {
        let strURL = Constants.kServerURL + strMethod
        
//            if (Constants.APP.nIsInterentAvailable == 1)
//            {
                Alamofire.request(strURL)
                    .responseJSON { (response) in
                         ZVProgressHUD.dismiss()
                        switch response.result
                        {
                        case .success(let res):
                            print(res)
                            completion(response, true)
                        case .failure(let error):
                            print(error)
                              completion(response, false)
                        }
                }
//            }else{
//                ZVProgressHUD.dismiss()
//               //self.displayAlertWithOk(message: "No Internet Connection")
//            }
    }
    */
    
    //-----------------------------------------------------------------------------------------------------------------
/*
    class func uploadFileToServer(_ dictData : Dictionary<String, String>, completion:@escaping(_ result : Any, _ isSuccess : Bool)->())
    {
     //   BaseViewController.checkIntenetConnection{ (isConnected) in
            
            if Constants.APP.nIsInterentAvailable == 1
            {
                let url = Bundle.main.url(forResource: "Default", withExtension: "png")
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                         multipartFormData.append(url!, withName: "image")
                },to:  Constants.kServerURL,
                   encodingCompletion: { encodingResult in
                    ZVProgressHUD.dismiss()
                     switch encodingResult {
                     case .success(let upload, _, _):
                         upload.responseJSON { response in
                             if let jsonResponse = response.result.value as? [String: Any] {
                                 print(jsonResponse)
                             }
                         }
                     case .failure(let encodingError):
                         print(encodingError)
                     }
                 }
             )
            }else{
                ZVProgressHUD.dismiss()
               // self.displayAlertWithOk(message: "No Internet Connection")
            }
   
    }
    */
    //-----------------------------------------------------------------------------------------------------------------
  
    /*
    class func displayAlertWithOk(message: String) {
        let alert = UIAlertController(title: Constants.kAPPName, message:message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
        })
     //   Constants.APP.window?.rootViewController?.present(alert, animated: true, completion: nil)
       // KCO.present(alert, animated: true, completion: nil)
    }
 */
    
    //---------------------------------------------------------------------------------------------
    
}



