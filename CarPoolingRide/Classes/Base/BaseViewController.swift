//
//  BaseViewController.swift
//  CarPoolingRide
//
//  Created by user on 22/11/19.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    let buttonBack = UIButton()
    @IBOutlet weak var viewBlur: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBack.frame = CGRect(x: 10, y: 40, width: 50, height: 40)
        buttonBack.backgroundColor = UIColor.clear
        buttonBack.setImage(UIImage(named: "back"), for: .normal)
        //buttonBack.addTarget(self, action: #selector(buttonBackClicked:) for: .touchUpInside)
        buttonBack.addTarget(self, action: #selector(buttonBackClicked), for: .touchUpInside)
        self.view.addSubview(buttonBack)
        // Do any additional setup after loading the view.
    //    Constants.mode
       // var device = UIDevice.Type
        
      
    }
    
    func setBorderForTextField(_ view : UIView){
         view.layer.cornerRadius = 12.0;
         view.layer.borderColor = UIColor.gray.cgColor;
         view.layer.borderWidth = 1.0
     }
 
     func showAlertWithOk(_ message: String) {
           let alert = UIAlertController(title: "Ride", message: message, preferredStyle: UIAlertController.Style.alert)
           alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }

    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }

    func pushViewController(_ strName : String){
        let vc = self.storyboard?.instantiateViewController(identifier: strName)
        self.navigationController?.pushViewController(vc!, animated: true)
    }

    @objc func buttonBackClicked(){
        self.navigationController?.popViewController(animated: true)
    }

}
