//
//  CardView.swift
//  CarPoolingRide
//
//  Created by Abhishek Patel on 18/02/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit

class CardView: UIView, UITextFieldDelegate {
    @IBOutlet weak var textFieldCreditCard: UITextField!
    @IBOutlet weak var textFieldDebitCard: UITextField!
    @IBOutlet weak var textFieldDate: UITextField!
    @IBOutlet weak var textFieldCVV: UITextField!
    
    @IBOutlet weak var viewCreditCard: UIView!
    @IBOutlet weak var viewDebitCard: UIView!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewCVV: UIView!
    @IBOutlet weak var imageViewVisaType: UIImageView!
    
    @IBOutlet weak var buttonCheckBox: UIButton!
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.viewCreditCard.layer.cornerRadius = 6.0
        self.viewDebitCard.layer.cornerRadius = 6.0
        self.viewDate.layer.cornerRadius = 6.0
        self.viewCVV.layer.cornerRadius = 6.0

        self.viewCVV.layer.borderColor = UIColor.lightGray.cgColor
        self.viewCVV.layer.borderWidth = 1

        self.viewDate.layer.borderColor = UIColor.lightGray.cgColor
        self.viewDate.layer.borderWidth = 1

        self.textFieldCreditCard.delegate = self
        
        self.viewDebitCard.layer.borderColor = UIColor.lightGray.cgColor
        self.viewDebitCard.layer.borderWidth = 1

        
        self.viewCreditCard.layer.borderColor = UIColor.lightGray.cgColor
        self.viewCreditCard.layer.borderWidth = 1

    }
     
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField == textFieldCVV {
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else  if textField == textFieldCreditCard {
            guard CardState(fromNumber: textFieldCreditCard.text!) != .invalid else {
                print("invaliddd")
                return true
            }

        }
        return true
    }

    
     
    
}
