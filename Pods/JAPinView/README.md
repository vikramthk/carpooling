# JAPinView

[![CI Status](https://img.shields.io/travis/JayachandraA/JAPinView.svg?style=flat)](https://travis-ci.org/JayachandraA/JAPinView)
[![Version](https://img.shields.io/cocoapods/v/JAPinView.svg?style=flat)](https://cocoapods.org/pods/JAPinView)
[![License](https://img.shields.io/cocoapods/l/JAPinView.svg?style=flat)](https://cocoapods.org/pods/JAPinView)
[![Platform](https://img.shields.io/cocoapods/p/JAPinView.svg?style=flat)](https://cocoapods.org/pods/JAPinView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JAPinView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'JAPinView'
```

## Author

JayachandraA, jayachandra.a@bluerose-tech.com

## License

JAPinView is available under the MIT license. See the LICENSE file for more info.
